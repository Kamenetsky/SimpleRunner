﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleRunner
{
    public interface IAccelerator
    {
        event Action OnComplete;

        float GetSpeed(float acceleration, float maxSpeed, float deltaTime);
    }
}
