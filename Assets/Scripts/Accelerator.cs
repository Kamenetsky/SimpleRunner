﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleRunner
{
    public abstract class Accelerator : IAccelerator
    {
        public event Action OnComplete = delegate { };

        protected float currentSpeed = 0f;
        protected float currentTime = 0f;
        bool isCompleted = false;

        public abstract float GetSpeed(float acceleration, float maxSpeed, float deltaTime);

        protected bool CheckComplete(float maxSpeed)
        {
            if (isCompleted)
                return true;

            if (currentSpeed >= maxSpeed)
            {
                isCompleted = true;
                OnComplete();
                return true;
            }

            return false;
        }
    }
}
