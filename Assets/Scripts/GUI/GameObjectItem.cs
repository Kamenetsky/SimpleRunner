﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SimpleRunner
{
    public class GameObjectItem : MonoBehaviour
    {
        [HideInInspector]
        public UnityEvent OnSelect;

        [SerializeField]
        Text caption;
        [SerializeField]
        Image icon;
        [HideInInspector]
        public int index;

        Toggle toggle;

        void Start()
        {
            toggle = GetComponent<Toggle>();
        }

        public void Init(string descr, Sprite sprite, int index)
        {
            this.index = index;
            caption.text = descr;
            icon.sprite = sprite;
        }

        public void OnStateChanged(bool val)
        {
            if (toggle.isOn)
                OnSelect.Invoke();
        }
    }
}
