﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SimpleRunner
{
    public class GameGUI : MonoBehaviour
    {
        public static System.Action<int, int> OnStartGame = delegate { };

        [SerializeField]
        GameObject startPanel;
        [SerializeField]
        GameObject gameOverPanel;

        [SerializeField]
        Transform levelsParent;
        [SerializeField]
        Transform figuresParent;
        [SerializeField]
        GameObject itemLevelPrefab;
        [SerializeField]
        GameObject itemFigurePrefab;

        int selectedLevel = 0;
        int selectedFigure = 0;

        public void ShowStartScreen(List<GameObjectInfo> levelsInfo, List<GameObjectInfo> figuresInfo)
        {
            startPanel.SetActive(true);
            for (int i = 0; i < levelsInfo.Count; ++i)
            {
                var it = Instantiate(itemLevelPrefab);
                it.transform.SetParent(levelsParent);
                it.SetActive(true);
                var goItem = it.GetComponent<GameObjectItem>();
                goItem.Init(levelsInfo[i].name, levelsInfo[i].icon, i);
                goItem.OnSelect.AddListener(delegate { OnSelectLevel(goItem); });
            }

            for (int i = 0; i < figuresInfo.Count; ++i)
            {
                var it = Instantiate(itemFigurePrefab);
                it.transform.SetParent(figuresParent);
                it.SetActive(true);
                var goItem = it.GetComponent<GameObjectItem>();
                goItem.Init(figuresInfo[i].name, figuresInfo[i].icon, i);
                goItem.OnSelect.AddListener(delegate { OnSelectFigure(goItem); });
            }
        }

        void OnSelectLevel(GameObjectItem it)
        {
            selectedLevel = it.index;
        }

        void OnSelectFigure(GameObjectItem it)
        {
            selectedFigure = it.index;
        }

        public void ShowGameOverScreen()
        {
            gameOverPanel.SetActive(true);
        }

        public void OnRestart()
        {
            gameOverPanel.SetActive(false);
            startPanel.SetActive(true);
        }

        public void OnStart()
        {
            startPanel.SetActive(false);
            OnStartGame(selectedLevel, selectedFigure);
        }
    }
}
