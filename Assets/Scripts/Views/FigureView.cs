﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace SimpleRunner
{
    public enum FigureType
    {
        Square,
        Circle,
        Triangle
    }

    public class FigureView : MonoBehaviour
    {
        public System.Action OnGameOver = delegate { };

        const string groundTag = "ground";
        const string obstacleTag = "obstacle";

        [SerializeField]
        FigureType figureType;

        [Header("Physics")]
        [SerializeField]
        Rigidbody2D body;
        [SerializeField]
        float acceleration;
        [SerializeField]
        float maxSpeed;
        [SerializeField]
        float jumpForce;

        IAccelerator accelerator;
        bool isAccelerationCompleted = false;
        bool isGrounded = true;

        [Header("Colors")]
        [SerializeField]
        Color jumpColor = Color.red;
        [SerializeField]
        Color initColor = Color.blue;
        SpriteRenderer image;

        bool gameIsOver = false;

        void Awake()
        {
            image = GetComponent<SpriteRenderer>();
            image.color = initColor;
            accelerator = AcceleratorFactory.Create(figureType);
            accelerator.OnComplete += OnCompleteAcceleration;
        }

        void OnDestroy()
        {
            accelerator.OnComplete -= OnCompleteAcceleration;
        }

        void Update()
        {
            if (gameIsOver)
                return;

            if (Input.GetMouseButtonDown(0) && isGrounded)
            {
                image.color = jumpColor;
                body.AddForce(Vector2.up * jumpForce);
                isGrounded = false;
            }
        }

        void FixedUpdate()
        {
            float speed = isAccelerationCompleted ? maxSpeed : accelerator.GetSpeed(acceleration, maxSpeed, Time.fixedDeltaTime);
            //Debug.Log(speed);
            body.velocity = new Vector2(speed, body.velocity.y);
        }

        void OnCompleteAcceleration()
        {
            isAccelerationCompleted = true;
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag(groundTag))
            {
                image.color = initColor;
                isGrounded = true;
            }
            else if (other.CompareTag(obstacleTag))
            {
                gameIsOver = true;
                OnGameOver();
            }
        }
    }
}
