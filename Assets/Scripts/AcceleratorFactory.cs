﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleRunner
{
    public class AcceleratorFactory
    {
        private static readonly Dictionary<FigureType, Type> map = new Dictionary<FigureType, Type>
		{
			{ FigureType.Circle, typeof(LinearAccelerator) },
			{ FigureType.Square, typeof(LinearAccelerator) },
			{ FigureType.Triangle,  typeof(HalfQuadAccelerator) }
		};

        public static IAccelerator Create(FigureType figureType)
        {
            if (!map.ContainsKey(figureType))
                throw new Exception(string.Format("No accelerator type specified for {0}. Check AcceleratorFactory", figureType.ToString()));
            Type acceleratorType = map[figureType];
            var res = (IAccelerator)Activator.CreateInstance(acceleratorType);
            return res;
        }
    }
}
