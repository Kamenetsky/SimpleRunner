﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleRunner
{
    public class LinearAccelerator : Accelerator
    {
        public override float GetSpeed(float acceleration, float maxSpeed, float deltaTime)
        {
            currentSpeed = acceleration * currentTime;
            currentTime += deltaTime;

            if (CheckComplete(maxSpeed))
                return maxSpeed;

            return currentSpeed;
        }
    }
}
