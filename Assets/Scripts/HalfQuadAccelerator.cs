﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleRunner
{
    public class HalfQuadAccelerator : Accelerator
    {
        public override float GetSpeed(float acceleration, float maxSpeed, float deltaTime)
        {
            if (currentSpeed > 0.5f * maxSpeed)
            {
                currentSpeed = acceleration * currentTime * currentTime;
            }
            else
            {
                currentSpeed = acceleration * currentTime;
            }
            currentTime += deltaTime;

            if (CheckComplete(maxSpeed))
                return maxSpeed;

            return currentSpeed;
        }
    }
}
