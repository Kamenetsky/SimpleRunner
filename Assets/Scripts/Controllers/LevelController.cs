﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleRunner
{
    public class LevelController : MonoBehaviour
    {
        [Header("Background parameters")]
        [SerializeField]
        Transform[] backObjects;
        [SerializeField]
        float backSpeed = 0.5f;
        [SerializeField]
        float backOffset;
        [SerializeField]
        Transform[] groundObjects;
        [SerializeField]
        float groundSpeed = 1f;
        [SerializeField]
        float groundOffset;

        [Header("Obstacle parameters")]
        [SerializeField]
        List<GameObject> obstaclePrefabs = new List<GameObject>();
        [SerializeField]
        [Range(35f, 100f)]
        float minObstacleDistance = 35f;
        [SerializeField]
        [Range(100f, 150f)]
        float maxObstacleDistance = 100f;

        [Header("Physics")]
        [SerializeField]
        Vector3 gravity = new Vector3(0f, -1f, 0f);

        List<GameObject> obstacles = new List<GameObject>();
        int startObstaclesCount = 3;

        Camera mainCamera;
        float prevCameraX;

        void Start()
        {
            mainCamera = Camera.main;
            Physics2D.gravity = gravity;
            for (int i = 0; i < startObstaclesCount; ++i)
                SpawnObstacle();
        }

        void OnDestroy()
        {
            foreach (var it in obstacles)
            {
                Destroy(it);
            }
            obstacles.Clear();
        }

        void Update()
        {
            float currCameraX = mainCamera.transform.position.x;
            foreach (var it in backObjects)
            {
                it.position += (currCameraX - prevCameraX) * Vector3.right * backSpeed;
                if (currCameraX - it.position.x > backOffset)
                    it.position += Vector3.right * backOffset * backObjects.Length;
            }

            foreach (var it in groundObjects)
            {
                it.position += (currCameraX - prevCameraX) * Vector3.right * groundSpeed;
                if (currCameraX - it.position.x > groundOffset)
                    it.position += Vector3.right * groundOffset * groundObjects.Length;
            }
            prevCameraX = currCameraX;

            if (obstacles.Count > 0 && currCameraX - obstacles[0].transform.position.x > backOffset)
            {
                Destroy(obstacles[0]);
                obstacles.RemoveAt(0);
                SpawnObstacle();
            }
        }

        void SpawnObstacle()
        {
            var go = Instantiate(obstaclePrefabs[Random.Range(0, obstaclePrefabs.Count)]);
            go.transform.position = new Vector3(obstacles.Count > 0 ? obstacles[obstacles.Count - 1].transform.position.x
                + Random.Range(minObstacleDistance, maxObstacleDistance) : backOffset, 0f, 0f);
            obstacles.Add(go);
        }

    }
}
