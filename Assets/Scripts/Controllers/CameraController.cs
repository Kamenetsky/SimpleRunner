﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleRunner
{
    public class CameraController : MonoBehaviour
    {
        Transform figure;

        [SerializeField]
        Vector3 offset = new Vector3(0f, 5f, 10f);

        public void FindTarget()
        {
            if (figure == null)
            {
                var obj = FindObjectOfType<FigureView>();
                if (obj == null)
                    return;

                figure = obj.transform;
            }
        }

        public void SetTarget(Transform target)
        {
            figure = target;
        }

        public void Reset()
        {
            figure = null;
        }

        void LateUpdate()
        {
            if (figure == null)
                return;

            Vector3 newPos = figure.position + offset;
            transform.position = newPos;
        }
    }
}
