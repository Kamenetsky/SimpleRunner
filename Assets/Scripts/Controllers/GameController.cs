﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleRunner
{
    [System.Serializable]
    public class GameObjectInfo
    {
        public string name;
        public Sprite icon;
        public GameObject prefab;
    }

    public class GameController : MonoBehaviour
    {

        [SerializeField]
        List<GameObjectInfo> levelsInfo = new List<GameObjectInfo>();
        [SerializeField]
        List<GameObjectInfo> figuresInfo = new List<GameObjectInfo>();

        GameObject levelController;
        FigureView figureView;

        [SerializeField]
        GameGUI gui;

        CameraController camController;

        void Awake()
        {
            GameGUI.OnStartGame += OnStartGame;
        }

        void Start()
        {
            camController = Camera.main.GetComponent<CameraController>();
            gui.ShowStartScreen(levelsInfo, figuresInfo);
        }

        void OnDestroy()
        {
            GameGUI.OnStartGame -= OnStartGame;
        }

        void OnStartGame(int levelIndex, int figureIndex)
        {
            levelController = Instantiate(levelsInfo[levelIndex].prefab);
            figureView = (Instantiate(figuresInfo[figureIndex].prefab) as GameObject).GetComponent<FigureView>();
            figureView.OnGameOver += OnGameOver;
            camController.SetTarget(figureView.transform);
        }

        void OnGameOver()
        {
            camController.Reset();
            figureView.OnGameOver -= OnGameOver;
            Destroy(figureView.gameObject);
            figureView = null;
            Destroy(levelController);
            levelController = null;
            gui.ShowGameOverScreen();
        }
    }
}
